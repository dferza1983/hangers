package com.personalsoft.hangers.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("test")
public class TestController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String matrix[][] = new String[3][3];
	
	
   @GetMapping(value = "hello", produces = MediaType.APPLICATION_JSON_VALUE)
   public String SayHello () {
	 return "Hola" ; 
   }
   
   @GetMapping(value = "bye", produces = MediaType.APPLICATION_JSON_VALUE)
   public String SayBye () {
	 return "Adios" ; 
   }
   
   @GetMapping(value = "validar_triki", produces = MediaType.APPLICATION_JSON_VALUE)
	public String validacionGanador(@RequestParam int posicionX, @RequestParam int posicionY, @RequestParam boolean type) {
		if (type) {
			this.matrix[posicionX][posicionY] = "X";
		} else {
			this.matrix[posicionX][posicionY] = "O";
		}
		return validacion(type);
	}
		 
   @GetMapping(value = "iniciarMatrixTriki", produces = MediaType.APPLICATION_JSON_VALUE)
   public String[][] iniciarMatrixTriki() {
	    this.matrix[0][0] = "-";
		this.matrix[0][1] = "-";
		this.matrix[0][2] = "-";
		this.matrix[1][0] = "-";
		this.matrix[1][1] = "-";
		this.matrix[1][2] = "-";
		this.matrix[2][0] = "-";
		this.matrix[2][1] = "-";
		this.matrix[2][2] = "-";
		return matrix;
	}
	   
   private String validacion(boolean type) {
	   String validador = null;
	   if (type) {
			if (this.matrix[0][0].equals("X") 
					&& this.matrix[0][1].equals("X") 
					&& this.matrix[0][2].equals("X")) {
				validador = "3";
			} 
			
			if (this.matrix[1][0].equals("X") 
					&& this.matrix[1][1].equals("X") 
					&& this.matrix[1][2].equals("X")) {
				validador = "3";
			}
			if (this.matrix[2][0].equals("X") 
					&& this.matrix[2][1].equals("X") 
					&& this.matrix[2][2].equals("X")) {
				validador = "3";
			}
			if (this.matrix[0][0].equals("X") 
					&& this.matrix[1][0].equals("X") 
					&& this.matrix[2][0].equals("X")) {
				validador = "3";
			}
			if (this.matrix[0][1].equals("X") 
					&& this.matrix[1][1].equals("X") 
					&& this.matrix[2][1].equals("X")) {
				validador = "3";
			}
			if (this.matrix[0][2].equals("X") 
					&& this.matrix[1][2].equals("X") 
					&& this.matrix[2][2].equals("X")) {
				validador = "3";
			}
			if (this.matrix[0][0].equals("X") 
					&& this.matrix[1][1].equals("X") 
					&& this.matrix[2][2].equals("X")) {
				validador = "3";
			}
			if (this.matrix[2][0].equals("X") 
					&& this.matrix[1][1].equals("X") 
					&& this.matrix[0][2].equals("X")) {
				validador = "3";
			}				
		} else {
			if (this.matrix[0][0].equals("O") 
					&& this.matrix[0][1].equals("O") 
					&& this.matrix[0][2].equals("O")) {
				 validador = "3";
			}
			if (this.matrix[1][0].equals("O") 
					&& this.matrix[1][1].equals("O") 
					&& this.matrix[1][2].equals("O")) {
				validador = "3";
			}
			if (this.matrix[2][0].equals("O") 
					&& this.matrix[2][1].equals("O") 
					&& this.matrix[2][2].equals("O")) {
				validador = "3";
			}
			if (this.matrix[0][0].equals("O") 
					&& this.matrix[1][0].equals("O") 
					&& this.matrix[2][0].equals("O")) {
				validador = "3";
			}
			if (this.matrix[0][1].equals("O") 
					&& this.matrix[1][1].equals("O") 
					&& this.matrix[2][1].equals("O")) {
				validador = "3";
			}
			if (this.matrix[0][2].equals("O") 
					&& this.matrix[1][2].equals("O") 
					&& this.matrix[2][2].equals("O")) {
				validador = "3";
			}
			if (this.matrix[0][0].equals("O") 
					&& this.matrix[1][1].equals("O") 
					&& this.matrix[2][2].equals("O")) {
				validador = "3";
			}
			if (this.matrix[2][0].equals("O") 
					&& this.matrix[1][1].equals("O") 
					&& this.matrix[0][2].equals("O")) {
				validador = "3";
			}
		}
	   
	   if (validador != "3"){
		   validador = "2"; 	   
	   }
	   
		return validador;
	}	     
  }
